export OS_AUTH_URL=
export OS_IDENTITY_API_VERSION=
export OS_PROJECT_NAME=
export OS_PRIVATE_KEY_PATH=
export OS_DOMAIN_NAME=
export OS_USERNAME=
export OS_KEY_PAIR_NAME=
export OS_INITIALS=
export OS_IMAGE=
export OS_FLAVOR=

export AGENT_AUTO_REGISTER_KEY=
export SONAR_USER=
export SONAR_PASSWORD=
